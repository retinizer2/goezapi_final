FROM golang:1.20-bullseye as builder
WORKDIR /opt/app
COPY . . 
RUN CGO_ENABLED=0  go build main.go

FROM scratch
WORKDIR /app
COPY --from=builder /opt/app/main . 
CMD ["./main"]