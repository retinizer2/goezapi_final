package models

import (
	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	Name     string
	Cost     int
	Rating   float64
	Count    uint
	Comments []Comment `gorm:"foreignKey:ProductRefer"`
}

type User struct {
	gorm.Model
	Name     string
	Surname  string
	Password string
	Mail     string
}

type Comment struct {
	gorm.Model
	Text         string
	Rating       float64
	UserRefer    uint
	User         User `gorm:"foreignKey:UserRefer"`
	ProductRefer uint
	Product      Product `gorm:"foreignKey:ProductRefer"`
}
