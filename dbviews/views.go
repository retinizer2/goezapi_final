package dbviews

import (
	"encoding/json"
	"log"
	"main/models"
	"net/http"

	"main/dbconn"

	"github.com/gorilla/mux"
)

// var DB *gorm.DB
// var errDB error
func CheckIfProductExists(productId string) bool {
	var product models.Product
	dbconn.DB.First(&product, productId)
	return product.ID != 0
}

func GetProducts(w http.ResponseWriter, r *http.Request) {
	var products []models.Product
	dbconn.DB.Find(&products)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(products)
}

func GetProductById(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["id"]
	if !CheckIfProductExists(key) {
		json.NewEncoder(w).Encode("Product Not Found!")
		return
	}
	var product models.Product
	dbconn.DB.First(&product, key)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(product)
}

func Purchasing(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["id"]
	if !CheckIfProductExists(key) {
		json.NewEncoder(w).Encode("Product Not Found!")
		return
	}
	var product models.Product
	dbconn.DB.First(&product, key)
	c := uint(product.Count)
	if c > 1 {
		product.Count = c
		dbconn.DB.Save(&product)
		log.Println("Product bought")
	} else {
		dbconn.DB.Delete(&product, key)
		json.NewEncoder(w).Encode("Product Deleted Successfully!")
	}

}

func Loopback(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var product models.Product
	json.NewDecoder(r.Body).Decode(&product)
	log.Println(product)
}

func AddProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var product models.Product
	json.NewDecoder(r.Body).Decode(&product)
	//log.Println()
	dbconn.DB.Create(&product)
	json.NewEncoder(w).Encode(product)
}

func updateRating(id string, rating float64) {
	var product models.Product
	dbconn.DB.First(&product, id)
	product.Rating = (rating + product.Rating) / 2
	dbconn.DB.Save(&product)
	log.Println("Rating Updated")
}

func PublishComment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	key := mux.Vars(r)["id"]
	var comment models.Comment
	json.NewDecoder(r.Body).Decode(&comment)
	dbconn.DB.Create(&comment)
	updateRating(key, comment.Rating)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var user models.User
	json.NewDecoder(r.Body).Decode(&user)
	dbconn.DB.Create(&user)
	json.NewEncoder(w).Encode(user)
}

//var timeout uint32

/*func SetConnection() {
	//DB, errDB = gorm.Open(sqlite.Open("db/gorm.db"), &gorm.Config{})
	for {
		DB, errDB = gorm.Open(postgres.Open(os.Getenv("DSN")), &gorm.Config{})
		if errDB != nil {
			log.Println("Postgres not ready...")
			timeout++
		} else {
			log.Println("Connected to Postgres!")
			DB.AutoMigrate(&models.Product{}, &models.User{}, &models.Comment{})
			return
		}
		if timeout > 5 {
			log.Panic(errDB)
		}
		time.Sleep(2 * time.Second)
	}
}*/
