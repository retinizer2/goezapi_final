package dbconn

import (
	"log"
	"main/models"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB
var errDB error
var timeout uint32

func SetConnection() {
	//DB, errDB = gorm.Open(sqlite.Open("db/gorm.db"), &gorm.Config{})
	for {
		DB, errDB = gorm.Open(postgres.Open(os.Getenv("DSN")), &gorm.Config{})
		if errDB != nil {
			log.Println("Postgres not ready...")
			timeout++
		} else {
			log.Println("Connected to Postgres!")
			DB.AutoMigrate(&models.Product{}, &models.User{}, &models.Comment{})
			return
		}
		if timeout > 5 {
			log.Panic(errDB)
		}
		time.Sleep(2 * time.Second)
	}
}
