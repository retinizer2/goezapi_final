package auth

import (
	//"main/models"
	"encoding/json"
	"log"
	"main/dbconn"
	"main/models"
	"net/http"

	"github.com/gorilla/mux"
)

var Authorized bool

type credentials struct {
	Mail     string `json:"mail"`
	Password string `json:"password"`
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	var users []models.User
	dbconn.DB.Find(&users)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var user models.User
	json.NewDecoder(r.Body).Decode(&user)
	dbconn.DB.Create(&user)
	json.NewEncoder(w).Encode(user)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	key := mux.Vars(r)["id"]
	var user models.User
	dbconn.DB.Delete(&user, key)
	json.NewEncoder(w).Encode("User Deleted Successfully!")
}

func PasswordFromEmail(mail string) string {
	var user models.User
	dbconn.DB.Where("Mail = ?", mail).First(&user)
	if user.ID != 0 {
		log.Printf("%s email exists", mail)
		return user.Password
	} else {
		return "111111111111111111111111111111111111111"
	}
}

func Auth(w http.ResponseWriter, r *http.Request) {
	var creds credentials
	json.NewDecoder(r.Body).Decode(&creds)
	log.Println(creds.Mail, creds.Password)
	log.Println(creds)
	if PasswordFromEmail(creds.Mail) == creds.Password {
		Authorized = true
		log.Printf("User Authorized as %s", creds.Mail)
		json.NewEncoder(w).Encode("Logged In")
	} else {
		json.NewEncoder(w).Encode("Wrong Email or Password")
	}
}
