package main

import (
	"log"
	"main/auth"
	"main/dbconn"
	"main/dbviews"
	"net/http"

	"github.com/gorilla/mux"
)

func HandleRequest() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/products", dbviews.GetProducts).Methods("GET")
	myRouter.HandleFunc("/products", dbviews.AddProduct).Methods("POST")
	myRouter.HandleFunc("/products", dbviews.Loopback).Methods("POST")
	myRouter.HandleFunc("/products/{id}/", dbviews.GetProductById).Methods("GET")
	myRouter.HandleFunc("/products/{id}/", dbviews.PublishComment).Methods(("POST"))
	myRouter.HandleFunc("/products/{id}/", dbviews.Purchasing).Methods("PUT")
	myRouter.HandleFunc("/auth", auth.CreateUser).Methods("POST")
	myRouter.HandleFunc("/auth/users", auth.GetUsers).Methods("GET")
	myRouter.HandleFunc("/auth/login", auth.Auth).Methods("POST")
	myRouter.HandleFunc("/auth/{id}", auth.DeleteUser).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8085", myRouter))
}

func main() {
	dbconn.SetConnection()
	HandleRequest()
}
